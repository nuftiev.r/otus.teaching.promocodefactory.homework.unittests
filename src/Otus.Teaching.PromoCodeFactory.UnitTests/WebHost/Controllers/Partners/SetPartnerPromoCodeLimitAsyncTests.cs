﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;


        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_IsReturnNotFound_NotFound()
        {
            var partnerId = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d91267d");

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync((Partner)null);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, It.IsAny<SetPartnerPromoCodeLimitRequest>());

            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_IsReturnBadRequest_BadRequest()
        {
            var partnerId = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d91267d");

            var partner = new Partner() 
            {
                Id = partnerId,
                IsActive = false
            };

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, It.IsAny<SetPartnerPromoCodeLimitRequest>());

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_CheckZeroingLimit_LimitZero()
        {
            var partnerId = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d91267d");

            var partner = new Partner()
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = 3,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit(){Id = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d912111"), CreateDate = DateTime.Parse("2023-04-29")}
                }
            };

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.Now
            };

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_DisablePreviousLimit_PreviousLimitCancelDateBeNull()
        {
            var partnerId = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d91267d");

            var partner = new Partner()
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = 3,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit(){Id = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d912111"), CreateDate = DateTime.Parse("2023-04-29")}
                }
            };

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.Now
            };

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var prevLimit = partner.PartnerLimits.FirstOrDefault(x => x.Id == Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d912111"));

            prevLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_CheckLimitGreaterThanZero_ReturnCreatedAtActionResult()
        {
            var partnerId = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d91267d");

            var partner = new Partner()
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = 3,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit(){Id = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d912111"), CreateDate = DateTime.Parse("2023-04-29")}
                }
            };

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.Now
            };

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_CheckSaveNewLimit_Verify()
        {
            var partnerId = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d91267d");

            var partner = new Partner()
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = 3,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit(){Id = Guid.Parse("76e8b7f8-c2e8-4b01-ac68-8f043d912111"), CreateDate = DateTime.Parse("2023-04-29")}
                }
            };

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.Now
            };

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner));
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
    }
}